package model.logic;

import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.vo.VOStation;
import model.vo.VOTrip;

public class DivvyTripsManager implements IDivvyTripsManager {

	DoublyLinkedList<VOTrip> viajes;
	DoublyLinkedList<VOStation> staciones;

	@SuppressWarnings("deprecation")
	public void loadStations(String stationsFile) throws IOException {
		CSVReader reader;
		try {
			reader = new CSVReader(new FileReader(stationsFile), ',');
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {
				VOStation f = new VOStation(nextLine[1], nextLine[2], Integer.parseInt(nextLine[3]),
						Integer.parseInt(nextLine[4]), Integer.parseInt(nextLine[5]), nextLine[6],
						Integer.parseInt(nextLine[7]));
				staciones.agregar(f);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	public void loadTrips(String tripsFile) {
		CSVReader reader;
		try {
			reader = new CSVReader(new FileReader(tripsFile), ',');
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {
				VOTrip f = new VOTrip(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[2],
						Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), Integer.parseInt(nextLine[5]),
						nextLine[6], Integer.parseInt(nextLine[7]), nextLine[8], nextLine[9], nextLine[10],
						Integer.parseInt(nextLine[11]));
				viajes.agregar(f);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public IDoublyLinkedList<VOTrip> getTripsOfGender(String gender) {
		DoublyLinkedList<VOTrip> res = new DoublyLinkedList<VOTrip>();
		viajes.empezar();
		while (viajes.termino()) {
			if (viajes.darValorActual().getGender().equals(gender)) {
				res.agregar(viajes.darValorActual());
			}
		}
		return res;
	}

	@Override
	public IDoublyLinkedList<VOTrip> getTripsToStation(int stationID) {
		DoublyLinkedList<VOTrip> res = new DoublyLinkedList<VOTrip>();
		viajes.empezar();
		while (viajes.termino()) {
			if (viajes.darValorActual().getTo_station_id() == stationID) {
				res.agregar(viajes.darValorActual());
			}
		}
		return res;
	}

	@Override
	public void loadServices(String stationsFile) {

	}
}