package model.vo;

public class VOStation {
	
	private String name;
	private String city;
	private int latitude;
	private int longitude;
	private int dpcapacity;
	private String online_date;
	private int id;
	
	
	
	public VOStation(String name, String city, int latitude, int longitude, int dpcapacity, String online_date,
			int id) {
		super();
		this.name = name;
		this.city = city;
		this.latitude = latitude;
		this.longitude = longitude;
		this.dpcapacity = dpcapacity;
		this.online_date = online_date;
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getLatitude() {
		return latitude;
	}
	public void setLatitude(int latitude) {
		this.latitude = latitude;
	}
	public int getLongitude() {
		return longitude;
	}
	public void setLongitude(int longitude) {
		this.longitude = longitude;
	}
	public int getDpcapacity() {
		return dpcapacity;
	}
	public void setDpcapacity(int dpcapacity) {
		this.dpcapacity = dpcapacity;
	}
	public String getOnline_date() {
		return online_date;
	}
	public void setOnline_date(String online_date) {
		this.online_date = online_date;
	}
	

}
