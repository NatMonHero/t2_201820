package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList<T> implements IDoublyLinkedList<T> {
	private NodoLista raiz;
	private NodoLista actual;
	private int total;

	public DoublyLinkedList() {
		raiz = null;
		actual = null;
		total = 0;

	}
	
	public T darValorActual(){
		return actual.elem;
	}

	public NodoLista getRaiz() {
		return raiz;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int p) {
		total = total + p;
	}

	public void agregar(T objeto) {
		NodoLista nuevo = new NodoLista(objeto);
		if (raiz != null) {
			while (actual.siguiente != null) {
				actual = actual.siguiente;
			}
			actual.setSiguiente(nuevo);
			nuevo.setAnterior(actual);
		} else {
			raiz = nuevo;
			actual = raiz;
		}
		total++;
	}

	public void eliminar(T objeto) {
		actual = raiz;
		if (actual != null && actual.equals(objeto)) {
			if (raiz.siguiente != null)
				raiz.siguiente.anterior = null;
			NodoLista a = raiz.siguiente;
			raiz.siguiente = null;
			raiz = a;
			actual = raiz;
			total--;
		} else {
			while (raiz.siguiente != null && !actual.equals(objeto)) {
				actual = actual.siguiente;
			}
			if (actual.equals(objeto)) {
				actual = actual.anterior;
				if (actual.siguiente.siguiente != null) {
					NodoLista t = actual.siguiente.siguiente;
					t.anterior = actual;
					actual.siguiente.siguiente = null;
					actual.siguiente.anterior = null;
					actual.siguiente = t;
					total--;
				} else {
					actual.siguiente.anterior = null;
					actual.siguiente = null;
				}
			}
		}

	}
	
	public boolean termino(){
		return actual!=null;
	}
	
	public void avanzar(){
		actual=actual.siguiente;
	}
	
	public void retroceder(){
		actual=actual.anterior;
	}
	
	public void empezar(){
		actual=raiz;
	}

	public NodoLista buscar(int p) {
		if (p > total-1)
			return null;
		else {
			int count=0;
			actual=raiz;
			while(count!=p){
				count++;
				actual=actual.siguiente;
			}
			return actual;
		}
	}

	public NodoLista buscar0(T objeto) {

		NodoLista respuesta = null;
		if (raiz != null) {
			actual = raiz.getSiguiente();
			if (raiz.getElem().equals(objeto)) {
				respuesta = raiz;
			} else {
				while (actual != null) {
					if (actual.getElem().equals(objeto)) {
						respuesta = actual;
					}
				}

			}

		}

		return respuesta;
	}

	class NodoLista {
		private T elem;
		NodoLista siguiente;
		NodoLista anterior;

		public NodoLista(T objeto) {
			elem = objeto;
		}

		public T getElem() {
			return elem;
		}

		public void setElem(T elem) {
			this.elem = elem;
		}

		public NodoLista getSiguiente() {
			return siguiente;
		}

		public void setSiguiente(NodoLista siguiente) {
			this.siguiente = siguiente;
		}

		public NodoLista getAnterior() {
			return anterior;
		}

		public void setAnterior(NodoLista anterior) {
			this.anterior = anterior;
		}
	}

	@Override
	public int getSize() {

		return total;
	}

	@Override
	public Iterator<T> iterator() {

		return null;
	}

	@Override
	public String toString() {
		String s = "";
		actual = raiz;
		if (actual != null)
			while (actual.siguiente != null) {
				s += actual.elem + " | ";
			}
		return s;
	}

}